
// Working from this tutorial: https://dev.to/adrai/the-progressive-guide-to-jquery-internationalization-i18n-using-i18next-3dc3

const lngs = {
  en: { nativeName: 'English' },
  da: { nativeName: 'Dansk' }
};

const rerender = () => {
  // start localizing, details:
  // https://github.com/i18next/jquery-i18next#usage-of-selector-function
  $('body').localize();
}

function toggleLang() {
    if (i18next.resolvedLanguage === "en"){
      i18next.changeLanguage("da");
      document.getElementById("languageSwitcher").textContent="English";
    }
    else{
      i18next.changeLanguage("en");
      document.getElementById("languageSwitcher").textContent="Dansk";
    }
    rerender();
}



$(function () {
  // use plugins and options as needed, for options, detail see
  // https://www.i18next.com
  i18next
    // detect user language
    // learn more: https://github.com/i18next/i18next-browser-languageDetector
    .use(i18nextBrowserLanguageDetector)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
      debug: true,
      fallbackLng: 'en',
      resources: {
        en: {
          translation: {
            headerMenus: {
              title: 'Playing with the Sun',
              home: 'Home',
              play: 'Play',
              pedagogy: 'Pedagogy',
              resources: 'Resources',    
            },  
              
            intro: {
              title: 'Playing with the Sun',
              subTitle: 'Designed for children and families, Playing with the Sun invites learners to follow their curiosity and build something powered by solar, wind, or muscles. In the process they develop their understanding of sustainable energy sources and their ability to create with them.'
            },
            sectionOne: {
              title: 'Play',
              constKitTitle: 'The Playing with the Sun Construction Kit',
              constKitDescription: 'Library, museum, and school educators can use the <a href="https://resources.playingwiththesun.org/Const-Kit-Overview/">Playing with the Sun construction kit</a> to offer learners a variety of activities in which they build something unique, based on their interests. </br> </br>        The Playing with the Sun construction kit is an open-source project under a Creative Commons License, which means that you can <a href = "https://resources.playingwiththesun.org/Const-Kit-Overview/">build one yourself</a> and contribute new activities, designs and ideas.',            
              activitiesTitle: 'Activities',
              activitiesDesc: 'Playing with the Sun activities are tinkering activities, which means they are designed to enable learners to build whatever they can imagine with a carefully curated set of play materials within a carefully designed set of constraints.',
              activityGallery1:'<strong>Drawing machines</strong> is an activity where learners create their own mark-making machines powered by solar energy or muscle power.',
              activityGallery2:'<strong>Cable Crawlers</strong> invites inventors to design their own unique cable car that drives along a string using energy from sunlight.',
              activityGallery3:'<strong>Solar bugs</strong> invites learners to make a solar powered critter that they can remote control using shadows and reflections from mirrors.',
            
            },
            pedagogy: {
                
              // It's set to read html, so you have to tag large lines of text with markup. 
              
              title: 'Pedagogy',
              bodyText: '<p>Children can create an intuitive sense for how sustainable energy works through playful tinkering and creative exploration. This lays a foundation on which to build a deeper understanding of energy that they’ll need in the decades to come.</p> <p>This video is from a small Playing with the Sun workshop in Berlin in January of 2023, and describes some of the pedagogical thinking behind the project. You can see more about the design of the workshop and the evidence of children\'s intelligence and creativity that emerged out of it on the <a href="https://www.eer.info/activities/playing-with-the-sun">EER Project Page</a>. EER refers to the <a href="https://www.eer.info/">Experimenting, Experiencing, Reflecting project</a> that helped start Playing with the Sun along with Aarhus Public Libraries.</p>',
                
              bodyText2: '<p>The pedagogy of Playing with the Sun takes its inspiration from a learning theory called constructionism, which is at the root of what’s known in science centers and museums around the world as Tinkering. As children design and build Tinkering projects together, they construct an understanding that’s meaningful and relevant because its driven by their curiosity. This project aims to create the conditions for curiosity about sustainable energy. We do this through the design of the activities, the <a href="https://resources.playingwiththesun.org/Const-Kit-Overview/">construction kit</a>, and the collective social interaction around them.</p> <p>You can read more about the thinking behind the pedagogy of Playing with the Sun in our <a href="PlayingwiththeSun-wp1.pdf">Working Paper #1</a>.'

            },            
            resources: {
              title: 'Resources',
              bodyText: 'You can see how to build the Playing with the Sun construction kit and learn more about the project on the <a href="https://resources.playingwiththesun.org">Playing with the Sun Resources website</a>.'
            },           
            generalUIelements: {
               downArrowMore: 'More',
               downArrowNext: 'Next',
            }
              
          }
        },
        da: {
          translation: {
            headerMenus: {
              title: 'Vi Leger med Solen',
              home: 'Hjem',
              play: 'Leg',
              pedagogy: 'Pædagogik',
              resources: 'Ressourcer',    
            },  
            intro: {
              title: 'Vi Leger med Solen',
              subTitle: 'Vi leger med solen er designet til børn og familier. Konceptet inviterer deltagerne til at følge deres nysgerrighed og bygge konstruktioner drevet af sol-, vind- eller muskelenergi. I processen udvikler de deres forståelse for bæredygtige energi i samspil med kreative processer.'
            },
            sectionOne: {
              title: 'Leg',
              constKitTitle: 'Vi leger med solen byggesæt',
              constKitDescription: '<a href="https://resources.playingwiththesun.org/Const-Kit-Overview/">Vi Leger med Solen Byggesættet</a> er tiltænkt formidlere på f.eks. biblioteker, museer og skoler, der med udgangspunkt i sættet kan tilbyde en palette af aktiviteter tilpasset deltagernes interesser og motivation. <p>Vi leger med solens byggesæt er open source under <a href="https://creativecommons.org/licenses/by-nc/4.0/">Creative Commons License</a>. Det betyder at du kan lave dit eget og gerne bidrage med nye aktiviteter, designs og ideer.',            
              activitiesTitle: 'Aktiviteter',
              activitiesDesc: 'Vi leger med solens forskellige aktiviteter er designet så de understøtter deltagerne i at bygge ud fra deres egen fantasi. Byggesættets materialer og rammen omkring de enkelte aktiviteter er omhyggeligt udvalgt, så de bedst understøtter deltagernes udforskning.',
              activityGallery1:'<strong>Mærkemaskiner</strong>  er en aktivitet, hvor deltagerne sætter deres eget aftryk med hjælp fra solens energi eller energien fra deres egen krop.',
              activityGallery2:'<strong>Kravlemaskiner</strong> inviterer deltagerne til at skabe en konstruktion, der kan kravle på en snor med hjælp fra solens energi eller energien fra deres egen krop.',
              activityGallery3:'<strong>Solinsekter</strong> inviterer deltagerne til ved hjælp af spejle at omdanne solens stråler (eller mangel på samme) til organiske bevægelser i små insektlignende konstruktioner.',
            
            },
            pedagogy: {
                
              title: 'Pædagogik',
                bodyText:'<p>Vi leger med solen har til formål at skabe gode betingelser for børns nysgerrighed rettet mod bæredygtig energi. Det understøttes gennem aktiviteterne og byggesættets design, og med fokus på det kollektive sociale samspil omkring dem.</p><p>Gennem tinkering og kreative undersøgelser får børnene mulighed for at opbygge en intuitiv fornemmelse for hvordan et eksempel på vedvarende, bæredygtig energi fungerer. Dette danner grundlaget for at bygge den dybere forståelse, de har brug for i de kommende årtier.</p> <p>Denne video er fra en vi leger med solen workshop i <a href="https://www.eer.info/activities/playing-with-the-sun">Berlin i januar 2023</a>, og viser et praksiseksempel af den pædagogiske tankegang bag projektet.</p>',

                bodyText2: 'Pædagogikken i vi leger med solen trækker på et konstruktivistisk læringssyn, som også er roden til det, der i videnskabscentre og museer rundt om i verden er kendt som Tinkering. Når børn designer og bygger Tinkering-projekter sammen, konstruerer de en forståelse, der er meningsfuld og relevant, fordi den er drevet af deres nysgerrighed.',

            },            
            resources: {
              title: 'Ressourcer',
              bodyText: 'Du kan se, hvordan du bygger Vi Leger med Solen-byggesættet og lære mere om projektet på <a href="https://resources.playingwiththesun.org">Playing with the Sun Resources-hjemmesiden</a>.'
            },
            generalUIelements: {
               downArrowMore: 'Mere',
               downArrowNext: 'Næste',
            }
          }
        }
      }
    }, (err, t) => {
      if (err) return console.error(err);

      // for options see
      // https://github.com/i18next/jquery-i18next#initialize-the-plugin
      jqueryI18next.init(i18next, $, { useOptionsAttr: true });

      // Show opposite language on button to whatever is current browser language 
        if (i18next.resolvedLanguage === "en") 
          document.getElementById("languageSwitcher").textContent="Dansk";
        else
          document.getElementById("languageSwitcher").textContent="English";
            
        });

      rerender();
    
});