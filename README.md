# PwtS-website

Website for Playing with the Sun, a project of Amos  Blanton, Ben Mardell, and Aarhus Public Libraries.

Site visible here: https://www.playingwiththesun.org/

Designed for children and families, Playing with the Sun invites learners to follow their curiosity and build something powered by solar, wind, or muscles. In the process they develop their understanding of alternative energy sources and their ability to create with them.

Based on the big picture template from the wonderful html5up - https://html5up.net/
